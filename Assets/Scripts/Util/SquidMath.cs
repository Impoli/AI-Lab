using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SquidMath
{
    public static float Smoothstep(float a, float b, float x)
    {
        x = Mathf.Clamp01((x - a) / (b - a));
        return x * x * (3f - 2f * x);
    }
}

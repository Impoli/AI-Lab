using UnityEngine;

public class Agent : MonoBehaviour
{
    #region Unity Messages
    protected void OnEnable()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _car = GetComponent<RCCP_CarController>();
        _input = GetComponentInChildren<RCCP_Input>();
        _car.canControl = true;
    }

    protected void Update()
    {
        _input.overridePlayerInputs = _useAI;
        _input.autoReverse = !_useAI;

        if (_useAI)
            _input.OverrideInputs(_inputs);
    }
    #endregion

    #region Public Methods

    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods

    #endregion

    #region Serialized Fields

    [Header("AI")]
    [SerializeField] protected bool _useAI;
    [SerializeField] protected RCCP_Inputs _inputs;

    [Header("Settings")]
    [SerializeField] protected float _width;
    [SerializeField] protected float _length;
    #endregion

    #region Protected Fields

    protected Rigidbody _rigidbody;
    protected RCCP_Input _input;
    protected RCCP_CarController _car;

    #endregion

    #region Private Fields
    #endregion

    #region Public Fields

    #endregion
}

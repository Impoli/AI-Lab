using System;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem.HID;
using UnityEngine.Windows;
using static UnityEditor.ShaderGraph.Internal.KeywordDependentCollection;
using static UnityEngine.Rendering.DebugUI;

public class FiniteStateAgent : Agent
{
    #region Unity Messages
    new protected void OnEnable()
    {
        base.OnEnable();
    }

    new protected void Update()
    {
        //Debug.Log($"{Time.frameCount} Update");
        base.Update();

        /* if (_currentLane != null)
         {//Debug.Log($"current lane: {_currentLane.name} {_currentLane.StartPoint} -> {_currentLane.EndPoint}");
         }*/


        UpdateHFSM();
        _speed = _rigidbody.velocity.magnitude * 3.6f;
    }

    private void OnDrawGizmosSelected()
    {
        /*Gizmos.color = Color.green;
        RaycastHit hit;
        int rayCount = 16;
        float step = _length / rayCount;
        Vector3 rayOrigin = transform.position + transform.right * _width / 2 + transform.forward * _length / 2;
        for (int i = 0; i < rayCount; i++)
        {
            if (Physics.Raycast(rayOrigin, transform.right, out hit, _closeDistance))
            {
                Gizmos.color = Color.red;
                foreach (var child in transform.GetComponentsInChildren<Transform>())
                {
                    if (child.gameObject != hit.transform.gameObject)
                    {
                        Gizmos.color = Color.green;
                        break;
                    }
                }
                Gizmos.DrawLine(rayOrigin, rayOrigin + transform.right * hit.distance);
            }
            else
                Gizmos.DrawLine(rayOrigin, rayOrigin + transform.right * _closeDistance);
            rayOrigin -= transform.forward * step;
        }
        
        rayOrigin = transform.position + transform.forward * _length / 2 - transform.right * _width / 2;
        if (_currentLane)
            rayOrigin = transform.position + transform.forward * _length / 2 - transform.right * _currentLane.Width / 2;
        rayCount = 8;
        step = _width / rayCount;
        if (_currentLane)
            step = _currentLane.Width / rayCount;
        for (int i = 0; i < rayCount; i++)
        {
            if (Physics.Raycast(rayOrigin, transform.forward, out hit, _viewDistance))
            {
                Gizmos.color = Color.red;
                Gizmos.DrawLine(rayOrigin, rayOrigin + transform.forward * hit.distance);
            }
            else
                Gizmos.DrawLine(rayOrigin, rayOrigin + transform.forward * _viewDistance);
            rayOrigin += transform.right * step;
        }*/
        if (_state != 0)
        {
            if (Enum.IsDefined(typeof(HFSM_State), _state))
                Debug.Log($"{name} current state: {_state}");
            else
                Debug.Log($"{name} current state: {_state}\n{StateToString()}");
        }
        if (_seekWaypoint)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(_waypoint, 2);
        }

        if (_seekWaypoint && _route != null)
        {
            List<Vector3> route = _route.GetRoute();
            Gizmos.color = Color.cyan;
            for (int i = 0; i < route.Count - 1; i++)
            {
                Vector3 start = route[i] + Vector3.up * 2;
                Vector3 end = route[i + 1] + Vector3.up * 2;
                Gizmos.DrawSphere(start, 1);
                Gizmos.DrawLine(start, end);
                if (i == route.Count - 2)
                    Gizmos.DrawSphere(end, 1);
            }
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(_target, 1.5f);
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, _closeDistance);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _mediumDistance);
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, _farDistance);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _viewDistance);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position + transform.forward * _length / 2 + transform.right * _width / 2, transform.position - transform.forward * _length / 2 + transform.right * _width / 2);
        Gizmos.DrawLine(transform.position + transform.forward * _length / 2 - transform.right * _width / 2, transform.position - transform.forward * _length / 2 - transform.right * _width / 2);
        Gizmos.DrawLine(transform.position + transform.forward * _length / 2 + transform.right * _width / 2, transform.position + transform.forward * _length / 2 - transform.right * _width / 2);
        Gizmos.DrawLine(transform.position - transform.forward * _length / 2 + transform.right * _width / 2, transform.position - transform.forward * _length / 2 - transform.right * _width / 2);
    }
    #endregion

    #region Public Methods

    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods

    #region HFSM Update

    /// <summary>
    /// Checks if agent is in state Driving AI and starts HFSM update.
    /// </summary>
    private void UpdateHFSM()
    {
        //Debug.Log($"{Time.frameCount} UpdateHFSM");
        // is driving ai?
        if (_useAI && !IsInState(HFSM_State.BASE_DRIVING_AI))
            EnterState(HFSM_State.BASE_DRIVING_AI);
        else if (!_useAI && IsInState(HFSM_State.BASE_DRIVING_AI))
            ExitState(HFSM_State.BASE_DRIVING_AI);

        // Update States, each state update updates its substates.
        if (IsInState(HFSM_State.BASE_DRIVING_AI))
        {
            UpdateRoles();
            UpdateDrivingAI();
        }
    }

    /// <summary>
    /// Updates _state and runs entry behaviour.
    /// </summary>
    /// <param name="enteredState">State to enter.</param>
    private void EnterState(HFSM_State enteredState)
    {
        //Debug.Log($"{Time.frameCount} EnterState {enteredState}");
        _state |= enteredState;
        switch (enteredState)
        {
            case HFSM_State.BASE_DRIVING_AI:
                EnterState(HFSM_State.BASE_IDLE);
                _minSpeed = 0;
                break;
            case HFSM_State.IDLE:
                _state = HFSM_State.IDLE;
                if (_currentLane == null)
                    _currentLane = GameManager.Instance.RoadGraph.GetClosestLane(transform.position);
                break;
            case HFSM_State.BASE_CIVILIAN:
            case HFSM_State.BASE_DRUNK:
            case HFSM_State.BASE_EMERGENCY:
            case HFSM_State.BASE_RACER:
                EnterState(HFSM_State.BASE_DRIVE_RANDOM);
                break;
            case HFSM_State.BASE_DRIVE_RANDOM:
                EnterState(HFSM_State.BASE_FOLLOW_LANE);
                break;
            case HFSM_State.BASE_FOLLOW_WAYPOINT:
                EnterState(HFSM_State.BASE_CALCULATE_ROUTE);
                break;
            case HFSM_State.BASE_CALCULATE_ROUTE:
                _route = new(transform.position, _waypoint);
                ExitState(HFSM_State.BASE_CALCULATE_ROUTE);
                EnterState(HFSM_State.BASE_FOLLOW_LANE);
                break;
            case HFSM_State.BASE_PARK:
                // TODO: find closest parking
                break;
            case HFSM_State.BASE_FOLLOW_LANE:
                _minSpeed = _maxSpeed / 3;
                //Debug.Log($"{Time.frameCount} current lane: {_currentLane}");
                if (_currentLane == null)
                    _currentLane = GameManager.Instance.RoadGraph.GetClosestLane(transform.position);
                break;
            case HFSM_State.BASE_SELECT_NEXT_LANE:
                SelectNextLane();
                break;
            case HFSM_State.BASE_MERGE_ONTO_LANE:
                // check if current lane is connected to next lane
                if (!_currentLane.EndIntersection.GetConnections(_currentLane).Contains(_nextLane))
                {
                    foreach (Lane parallelLane in _currentLane.ParallelLanes)
                    {
                        if (parallelLane.EndIntersection.GetConnections(parallelLane).Contains(_nextLane))
                        {
                            _currentLane = parallelLane;
                            break;
                        }
                    }
                }
                break;
            case HFSM_State.BASE_APPROACH_INTERSECTION:
                _maxSpeed = _currentLane.EndIntersection.SpeedLimit;
                _minSpeed = 0;
                _currentLane.EndIntersection.ApproachingAgents.Add(this);
                break;
            case HFSM_State.BASE_CROSS_INTERSECTION:
                _minSpeed = 10;
                if (_nextLane == null)
                {
                    ExitState(HFSM_State.BASE_CROSS_INTERSECTION);
                    EnterState(HFSM_State.BASE_FOLLOW_LANE);
                }
                else
                    _enterIntersection = true;
                break;
            case HFSM_State.BASE_EVADE_OBSTACLE:
                _lastPositionEvade = transform.position;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Updates _state and runs exit behaviour
    /// </summary>
    /// <param name="exitedState">State to exit.</param>
    private void ExitState(HFSM_State exitedState)
    {
        //Debug.Log($"{Time.frameCount} ExitState {exitedState}");
        _state &= ~exitedState;
        switch (exitedState)
        {
            case HFSM_State.BASE_CIVILIAN:
            case HFSM_State.BASE_DRUNK:
            case HFSM_State.BASE_EMERGENCY:
            case HFSM_State.BASE_RACER:
                EnterState(HFSM_State.IDLE);
                break;
            case HFSM_State.BASE_PARK:
                _drive = false;
                _park = false;
                EnterState(HFSM_State.IDLE);
                break;
            case HFSM_State.BASE_MAKE_WAY:
                EnterState(HFSM_State.BASE_DRIVE_RANDOM);
                break;
            case HFSM_State.BASE_CROSS_INTERSECTION:
                _currentLane.EndIntersection.ApproachingAgents.Remove(this);
                _currentLane = _nextLane;
                _nextLane = null;
                break;
            case HFSM_State.BASE_CHASE:
                EnterState(HFSM_State.BASE_DRIVE_RANDOM);
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Updates Role States
    /// </summary>
    private void UpdateRoles()
    {
        //Debug.Log($"{Time.frameCount} UpdateRole");

        // is idle?
        if (!_drive && !IsInState(HFSM_State.IDLE))
            EnterState(HFSM_State.IDLE);
        else if (_drive && IsInState(HFSM_State.IDLE))
            ExitState(HFSM_State.BASE_IDLE);

        // is civilian?
        if (_isCivilian && !IsInState(HFSM_State.BASE_CIVILIAN))
            EnterState(HFSM_State.BASE_CIVILIAN);
        else if (!_isCivilian && IsInState(HFSM_State.BASE_CIVILIAN))
            ExitState(HFSM_State.BASE_CIVILIAN);

        // is drunk?
        if (_isDrunk && !IsInState(HFSM_State.BASE_DRUNK))
            EnterState(HFSM_State.BASE_DRUNK);
        else if (!_isDrunk && IsInState(HFSM_State.BASE_DRUNK))
            ExitState(HFSM_State.BASE_DRUNK);

        // is emergency?
        if (_isEmergency && !IsInState(HFSM_State.BASE_EMERGENCY))
            EnterState(HFSM_State.BASE_EMERGENCY);
        else if (!_isEmergency && IsInState(HFSM_State.BASE_EMERGENCY))
            ExitState(HFSM_State.BASE_EMERGENCY);

        // is racer?
        if (_isRacer && !IsInState(HFSM_State.BASE_RACER))
            EnterState(HFSM_State.BASE_RACER);
        else if (!_isRacer && IsInState(HFSM_State.BASE_RACER))
            ExitState(HFSM_State.BASE_RACER);
    }

    private void UpdateDrivingAI()
    {
        //Debug.Log($"{Time.frameCount} UpdateDrivingAI");
        if (IsInState(HFSM_State.IDLE))
            UpdateIdle();
        else if (IsInState(HFSM_State.CIVILIAN))
            UpdateCivilian();
        else if (IsInState(HFSM_State.DRUNK))
            UpdateDrunk();
        else if (IsInState(HFSM_State.EMERGENCY))
            UpdateEmergency();
        else if (IsInState(HFSM_State.RACER))
            UpdateRacer();
        else
        {
            Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
            EnterState(HFSM_State.IDLE);
        }
    }

    /// <summary>
    /// Update for agent in idle state
    /// </summary>
    private void UpdateIdle()
    {
        //Debug.Log($"{Time.frameCount} UpdateIdle");
        Accelerate(0);
        if (_drive)
            ExitState(HFSM_State.BASE_IDLE);
    }

    /// <summary>
    /// Update for agent in civilian state
    /// </summary>
    private void UpdateCivilian()
    {
        //Debug.Log($"{Time.frameCount} UpdateCivilian");
        if (_currentLane != null)
            _maxSpeed = _currentLane.SpeedLimit;
        if (IsInState(HFSM_State.CIVILIAN_RANDOM))
            UpdateDriveRandomly();
        else if (IsInState(HFSM_State.CIVILIAN_WAYPOINT))
            UpdateFollowWaypoint();
        else if (IsInState(HFSM_State.CIVILIAN_PARK))
            UpdatePark();
        else if (IsInState(HFSM_State.CIVILIAN_MAKE_WAY))
            UpdateMakeWay();
        else
        {
            Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
            EnterState(HFSM_State.IDLE);
        }
    }

    /// <summary>
    /// Update for agent in drunk state
    /// </summary>
    private void UpdateDrunk()
    {
        //Debug.Log($"{Time.frameCount} UpdateDrunk");

        if (_currentLane != null)
        {
            _minSpeed = UnityEngine.Random.Range(0f, _currentLane.SpeedLimit);
            _maxSpeed = UnityEngine.Random.Range(_currentLane.SpeedLimit, _currentLane.SpeedLimit * 2);
        }
        if (IsInState(HFSM_State.DRUNK_RANDOM))
            UpdateDriveRandomly();
        else if (IsInState(HFSM_State.DRUNK_WAYPOINT))
            UpdateFollowWaypoint();
        else if (IsInState(HFSM_State.DRUNK_PARK))
            UpdatePark();
        else if (IsInState(HFSM_State.DRUNK_MAKE_WAY))
            UpdateMakeWay();
        else
        {
            Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
            EnterState(HFSM_State.IDLE);
        }
    }

    /// <summary>
    /// Update for agent in emergency state
    /// </summary>
    private void UpdateEmergency()
    {
        //Debug.Log($"{Time.frameCount} UpdateEmergency");
        CheckDrivers();
        if (_suspect != null)
        {
            _maxSpeed = 200;
            _minSpeed = 20;
            _chase = true;
            EnterState(HFSM_State.BASE_CHASE);
        }
        else if (_suspect == null && IsInState(HFSM_State.BASE_CHASE))
            ExitState(HFSM_State.BASE_CHASE);
        else if (_currentLane != null)
            _maxSpeed = _currentLane.SpeedLimit;
        if (IsInState(HFSM_State.EMERGENCY_RANDOM))
            UpdateDriveRandomly();
        else if (IsInState(HFSM_State.EMERGENCY_WAYPOINT))
            UpdateFollowWaypoint();
        else if (IsInState(HFSM_State.EMERGENCY_PARK))
            UpdatePark();
        else if (IsInState(HFSM_State.EMERGENCY_CHASE))
            UpdateChase();
        else
        {
            Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
            EnterState(HFSM_State.IDLE);
        }
    }

    /// <summary>
    /// Update for agent in racer state
    /// </summary>
    private void UpdateRacer()
    {
        //Debug.Log($"{Time.frameCount} UpdateRacer");
        CheckDrivers();
        _maxSpeed = 100;
        _minSpeed = _currentLane.SpeedLimit;
        if (_officer != null)
            _flee = true;
        else
        {
            _flee = false;
            ExitState(HFSM_State.BASE_FLEE);
            if (_seekWaypoint)
                EnterState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            else
                EnterState(HFSM_State.BASE_DRIVE_RANDOM);
        }
        if (_currentLane != null)
            _minSpeed = _currentLane.SpeedLimit * 0.5f;
        if (IsInState(HFSM_State.RACER_RANDOM))
            UpdateDriveRandomly();
        else if (IsInState(HFSM_State.RACER_WAYPOINT))
            UpdateFollowWaypoint();
        else if (IsInState(HFSM_State.RACER_PARK))
            UpdatePark();
        else if (IsInState(HFSM_State.RACER_FLEE))
            UpdateFlee();
        else
        {
            Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
            EnterState(HFSM_State.IDLE);
        }
    }

    /// <summary>
    /// Update for agent in drive randomly state
    /// </summary>
    private void UpdateDriveRandomly()
    {
        //Debug.Log($"{Time.frameCount} UpdateDriveRandomly");
        if (_makeWay)
        {
            ExitState(HFSM_State.BASE_DRIVE_RANDOM);
            EnterState(HFSM_State.BASE_MAKE_WAY);
        }
        else if (_chase)
        {
            ExitState(HFSM_State.BASE_DRIVE_RANDOM);
            EnterState(HFSM_State.BASE_CHASE);
        }
        else if (_flee)
        {
            ExitState(HFSM_State.BASE_DRIVE_RANDOM);
            EnterState(HFSM_State.BASE_FLEE);
        }
        else if (_park)
        {
            ExitState(HFSM_State.BASE_DRIVE_RANDOM);
            EnterState(HFSM_State.BASE_PARK);
        }
        else if (_seekWaypoint)
        {
            ExitState(HFSM_State.BASE_DRIVE_RANDOM);
            EnterState(HFSM_State.BASE_FOLLOW_WAYPOINT);
        }
        else
        {
            if (IsInState(HFSM_State.BASE_FOLLOW_LANE))
                UpdateFollowLane();
            else if (IsInState(HFSM_State.BASE_EVADE_OBSTACLE))
                UpdateEvadeObstacle();
            else if (IsInState(HFSM_State.BASE_OVERTAKE))
                UpdateOvertake();
            else if (IsInState(HFSM_State.BASE_MERGE_ONTO_LANE))
                UpdateMergeLane();
            else if (IsInState(HFSM_State.BASE_APPROACH_INTERSECTION))
                UpdateApproachIntersection();
            else if (IsInState(HFSM_State.BASE_WAIT_AT_INTERSECTION))
                UpdateWaitIntersection();
            else if (IsInState(HFSM_State.BASE_CROSS_INTERSECTION))
                UpdateCrossIntersection();
            else
            {
                Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
                EnterState(HFSM_State.IDLE);
            }
        }
    }

    /// <summary>
    /// Update for agent in follow waypoint state
    /// </summary>
    private void UpdateFollowWaypoint()
    {
        //Debug.Log($"{Time.frameCount} UpdateFollowWaypoint");
        float delta = (_waypoint - transform.position).magnitude;
        if (delta < _mediumDistance && !IsInState(HFSM_State.BASE_WAYPOINT_REACHED))
        {
            EnterState(HFSM_State.BASE_WAYPOINT_REACHED);
        }
        else if (_makeWay)
        {
            ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            EnterState(HFSM_State.BASE_MAKE_WAY);
        }
        else if (_chase)
        {
            ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            EnterState(HFSM_State.BASE_CHASE);
        }
        else if (_flee)
        {
            ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            EnterState(HFSM_State.BASE_FLEE);
        }
        else if (_park)
        {
            ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            EnterState(HFSM_State.BASE_PARK);
        }
        else if (!_seekWaypoint)
        {
            ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            EnterState(HFSM_State.BASE_DRIVE_RANDOM);
        }
        else
        {
            if (IsInState(HFSM_State.BASE_WAYPOINT_REACHED))
                UpdateWaypointReached();
            else if (IsInState(HFSM_State.BASE_CALCULATE_ROUTE))
                UpdateCalculateRoute();
            else if (IsInState(HFSM_State.BASE_FOLLOW_LANE))
                UpdateFollowLane();
            else if (IsInState(HFSM_State.BASE_EVADE_OBSTACLE))
                UpdateEvadeObstacle();
            else if (IsInState(HFSM_State.BASE_OVERTAKE))
                UpdateOvertake();
            else if (IsInState(HFSM_State.BASE_MERGE_ONTO_LANE))
                UpdateMergeLane();
            else if (IsInState(HFSM_State.BASE_APPROACH_INTERSECTION))
                UpdateApproachIntersection();
            else if (IsInState(HFSM_State.BASE_WAIT_AT_INTERSECTION))
                UpdateWaitIntersection();
            else if (IsInState(HFSM_State.BASE_CROSS_INTERSECTION))
                UpdateCrossIntersection();
            else
            {
                Debug.LogError($"Invalid state {Convert.ToString((int)_state, toBase: 2)}\n{StateToString()}");
                EnterState(HFSM_State.IDLE);
            }
        }
    }

    /// <summary>
    /// Update for agent in make way state
    /// </summary>
    private void UpdateMakeWay()
    {
        //Debug.Log($"{Time.frameCount} UpdateMakeWay");
        // TODO: make way behavior
    }

    /// <summary>
    /// Update for agent in park state
    /// </summary>
    private void UpdatePark()
    {
        //Debug.Log($"{Time.frameCount} UpdatePark");
        // TODO: Parking Behaviour
    }

    /// <summary>
    /// Update for agent in chase state
    /// </summary>
    private void UpdateChase()
    {
        //Debug.Log($"{Time.frameCount} UpdateChase");
        float delta;

        foreach (FiniteStateAgent agent in GameManager.Instance.Agents)
        {
            Vector3 direction = transform.position - agent.transform.position;
            delta = direction.magnitude;
            if (delta < agent.ViewDistance && Vector3.Dot(direction, transform.forward) < 0 && !agent.IsRacer && !agent.IsEmergency)
                agent.MakeWay = true;
            else
                agent.MakeWay = false;
        }

        _waypoint = _suspect.transform.position;
        _target = _waypoint;
        delta = (transform.position - _target).magnitude;

        // suspect is fleeing
        if (delta > _viewDistance)
        {
            if (!IsInState(HFSM_State.BASE_FOLLOW_WAYPOINT))
                EnterState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            UpdateFollowWaypoint();

            delta = Time.realtimeSinceStartup - _suspectLastSeen;
            // suspect got away
            if (delta > _chaseSearchTime)
            {
                _suspect = null;
                _chase = false;
                ExitState(HFSM_State.BASE_CHASE);
                ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
            }
            return;
        }
        else
        {
            if (IsInState(HFSM_State.BASE_FOLLOW_WAYPOINT))
                ExitState(HFSM_State.BASE_FOLLOW_WAYPOINT);
        }

        // suspect in view
        if (delta < _viewDistance)
        {
            _currentLane = GameManager.Instance.RoadGraph.GetClosestLane(transform.position);
            _target = _currentLane.GetClosestPoint(_target);

            if (delta < _mediumDistance)
            {
                _target = _waypoint;
            }
        }

        _minDistance = _length / 2;

        Seek();
    }

    /// <summary>
    /// Update for agent in flee state
    /// </summary>
    private void UpdateFlee()
    {
        //Debug.Log($"{Time.frameCount} UpdateFlee");
        // distance to intersection < approach distance?
        float delta = (_currentLane.EndIntersection.transform.position - transform.position).magnitude;
        if (delta < _farDistance)
        {
            SelectNextLane();
        }

        _target = _currentLane.EndPoint;
        Vector3 direction = transform.position - _officer.transform.position;
        delta = direction.magnitude;
        if (delta < _mediumDistance)
        {
            _target = -direction.normalized * _farDistance;
        }

        // check for objects ahead
        int ahead = CheckAhead();
        if (ahead != 0)
        {
            if (_currentLane && _currentLane.OpposingLanes.Count > 0)
                _target = _currentLane.OpposingLanes.First().StartPoint;
            if (_currentLane && _currentLane.ParallelLanes.Count > 0)
                _target = _currentLane.ParallelLanes.First().EndPoint;
        }

        if (_currentLane.OpposingLanes.Count > 0)
        {
            float distanceCurrent = (_currentLane.EndPoint - transform.position).magnitude;
            float distanceOpposing = (_currentLane.OpposingLanes.First().EndPoint - transform.position).magnitude;
            if (distanceOpposing < distanceCurrent)
            {
                _currentLane = _currentLane.OpposingLanes.First();
            }
        }

        _target = _currentLane.GetClosestPoint(_target);
        _minDistance = 0;
        Seek();
    }

    /// <summary>
    /// Update for agent in follow lane state
    /// </summary>
    private void UpdateFollowLane()
    {
        //Debug.Log($"{Time.frameCount} UpdateFollowLane");
        // distance to intersection < approach distance?
        _minDistance = _closeDistance;
        float delta = (_currentLane.EndIntersection.transform.position - transform.position).magnitude;
        if (delta < _viewDistance)
        {
            ExitState(HFSM_State.BASE_FOLLOW_LANE);
            EnterState(HFSM_State.BASE_SELECT_NEXT_LANE);
            return;
        }


        // follow lane
        delta = (_currentLane.GetClosestPoint(transform.position) - transform.position).magnitude;
        if (delta > _closeDistance)
            _target = _currentLane.GetClosestPoint(transform.position);
        else
            _target = _currentLane.EndPoint;

        // check for objects ahead
        int ahead = CheckAhead();
        if (ahead == 1)
        {
            if (_currentLane.ParallelLanes.Count > 0)
            {
                ExitState(HFSM_State.BASE_FOLLOW_LANE);
                EnterState(HFSM_State.BASE_OVERTAKE);
                return;
            }
        }
        else if (ahead == 2)
        {
            ExitState(HFSM_State.BASE_FOLLOW_LANE);
            EnterState(HFSM_State.BASE_EVADE_OBSTACLE);
            return;
        }

        if (ahead != 0)
        {
            _minSpeed = 0;
        }
        else if (_target == _currentLane.StartPoint)
            _minSpeed = _currentLane.SpeedLimit * 0.9f;
        else
            _minSpeed = _currentLane.SpeedLimit * 0.5f;


        Seek();
    }

    /// <summary>
    /// Checks for objects ahead and takes action accordingly
    /// </summary>
    /// <returns>
    /// 0 if nothing is ahead.
    /// 1 if vehicle is ahead.
    /// 2 if obstacle is ahead.
    /// </returns>
    private int CheckAhead()
    {

        float isObjectAhead = IsObjectAhead();
        bool carAhead = isObjectAhead > 0;
        bool obstacleAhead = isObjectAhead < 0;
        float speed = isObjectAhead;

        if (carAhead && Mathf.Abs(_speed - speed) < 20)
        {
            _maxSpeed = speed;
            return 1;
        }
        else if (obstacleAhead)
            return 2;

        return 0;
    }

    /// <summary>
    /// Casts rays ahead and checks if vehicle or object is too close.
    /// </summary>
    /// <returns>
    /// speed of vehicle ahead if vehicle is ahead.
    /// -hit distance if obstacle is ahead.
    /// 0 if nothing is ahead.
    /// </returns>
    private float IsObjectAhead()
    {
        // Cast rays to check for obstacles and agents ahead.
        Vector3 rayOrigin = transform.position + transform.forward * _length / 2 - transform.right * _width / 2;
        if (_currentLane)
            rayOrigin = transform.position + transform.forward * _length / 2 - transform.right * _currentLane.Width / 2;
        RaycastHit hit;
        int rayCount = 8;
        float step = _width / rayCount;
        if (_currentLane)
            step = _currentLane.Width / rayCount;
        for (int i = 0; i < rayCount; i++)
        {
            if (Physics.Raycast(rayOrigin, transform.forward, out hit, _viewDistance))
            {
                foreach (var child in transform.GetComponentsInChildren<Transform>())
                {
                    if (child.gameObject == hit.transform.gameObject)
                        return 0;
                }

                if (_currentLane.GetDistanceToCenter(hit.point) > _currentLane.Width / 2)
                    continue;

                _target = _currentLane.GetClosestPoint(hit.point);

                // if agent too close react
                if (hit.distance < _mediumDistance && hit.transform.CompareTag("Agent"))
                {
                    FiniteStateAgent other = hit.rigidbody.GetComponent<FiniteStateAgent>();
                    return other.Speed;
                }
                // if obstacle is too close evade
                else if (hit.distance < _farDistance && hit.transform.CompareTag("Obstacle"))
                {
                    return -hit.distance;
                }
            }
            rayOrigin += transform.right * step;
        }
        return 0;
    }

    /// <summary>
    /// Update for agent in evade obstacle state
    /// </summary>
    private void UpdateEvadeObstacle()
    {
        //Debug.Log($"{Time.frameCount} UpdateEvadeObstacle");

        // check for parallel lanes
        if (_currentLane.ParallelLanes.Count > 0)
        {
            _currentLane = _currentLane.ParallelLanes[0];
            ExitState(HFSM_State.BASE_EVADE_OBSTACLE);
            EnterState(HFSM_State.BASE_MERGE_ONTO_LANE);
            return;
        }

        _target = _currentLane.EndPoint;

        // check if obstacle is ahead
        float objectAhead = IsObjectAhead();
        bool obstacleAhead = objectAhead < 0;
        float distance = -objectAhead;
        if (obstacleAhead)
        {
            _target = transform.position + transform.forward * distance;
            float targetDistanceToLane = (_target - _currentLane.GetClosestPoint(_target)).magnitude;
            if (targetDistanceToLane > _currentLane.Width / 2)
            {
                obstacleAhead = false;
            }
        }

        // check for oncoming traffic
        bool oncoming = false;
        if (obstacleAhead && distance < _mediumDistance && _currentLane.OpposingLanes.Count > 0)
        {
            Vector3 rayOrigin = _currentLane.OpposingLanes[0].GetClosestPoint(transform.position);
            RaycastHit hit;
            if (Physics.Raycast(rayOrigin, transform.forward, out hit, _viewDistance))
            {
                oncoming = true;
            }
        }

        // pass obstacle
        if (!oncoming && distance < _mediumDistance && _currentLane.OpposingLanes.Count > 0)
        {
            float distanceCurrentLane = (transform.position - _currentLane.GetClosestPoint(transform.position)).magnitude;
            float distanceOppositeLane = (transform.position - _currentLane.OpposingLanes[0].GetClosestPoint(transform.position)).magnitude;

            if (distanceCurrentLane < distanceOppositeLane)
                _target = _currentLane.OpposingLanes[0].GetClosestPoint(transform.position);
            else
            {
                _target = _currentLane.OpposingLanes[0].StartPoint;

                // Cast rays to check for obstacles and agents ahead.
                Vector3 rayOrigin = transform.position + transform.right * _width / 2 + transform.forward * _length / 2;
                RaycastHit hit;
                int rayCount = 16;
                float step = _length / rayCount;
                bool obstaclePassed = true;
                for (int i = 0; i < rayCount; i++)
                {
                    if (Physics.Raycast(rayOrigin, transform.right, out hit, _closeDistance))
                    {
                        if (name == hit.transform.name)
                            Debug.LogError($"{name} found {hit.transform.name} at {hit.distance}");
                        obstaclePassed = false;
                        _lastPositionEvade = transform.position;
                        //Debug.Log($"obstacle {hit.transform.name} at distance {hit.distance}");
                    }
                    rayOrigin -= transform.forward * step;
                }

                float distanceLastPosition = (_lastPositionEvade - transform.position).magnitude;
                if (obstaclePassed && distanceLastPosition > 1)
                {
                    ExitState(HFSM_State.BASE_EVADE_OBSTACLE);
                    EnterState(HFSM_State.BASE_MERGE_ONTO_LANE);
                    return;
                }
            }
        }
        _minDistance = _closeDistance;
        Seek();
    }

    /// <summary>
    /// Update for agent in overtake state
    /// </summary>
    private void UpdateOvertake()
    {
        //Debug.Log($"{Time.frameCount} UpdateOvertake");
        if (_currentLane.ParallelLanes.Count > 0)
            _currentLane = _currentLane.ParallelLanes[0];
        ExitState(HFSM_State.BASE_OVERTAKE);
        EnterState(HFSM_State.BASE_MERGE_ONTO_LANE);
    }

    /// <summary>
    /// Update for agent in merge lane state
    /// </summary>
    private void UpdateMergeLane()
    {
        //Debug.Log($"{Time.frameCount} UpdateMergeLane");
        Vector3 closestPoint = _currentLane.GetClosestPoint(transform.position);
        Vector3 laneDirection = (_currentLane.EndPoint - _currentLane.StartPoint).normalized;
        _target = closestPoint + laneDirection * _mediumDistance;
        _minDistance = _closeDistance;
        // if on current lane switch state
        float delta = _currentLane.GetDistanceToCenter(transform.position);
        if (delta < _width)
        {
            ExitState(HFSM_State.BASE_MERGE_ONTO_LANE);
            EnterState(HFSM_State.BASE_APPROACH_INTERSECTION);
        }
        // else check if lane is clear
        else
        {
            Vector3 rayDirection = (closestPoint - transform.position).normalized;
            Vector3 rayOrigin = transform.position + rayDirection * _width / 2 - transform.forward * _length / 2;
            RaycastHit hit;
            int rayCount = 8;
            float step = _length / rayCount;
            for (int i = 0; i < rayCount; i++)
            {
                if (Physics.Raycast(rayOrigin, transform.forward, out hit, delta))
                {
                    bool hasHit = true;
                    if (hit.transform.CompareTag("Agent"))
                    {
                        foreach (var child in transform.GetComponentsInChildren<Transform>())
                        {
                            if (child.gameObject == hit.transform.gameObject)
                                hasHit = false;
                        }
                        if (!hasHit)
                            break;
                        _maxSpeed *= SquidMath.Smoothstep(0, _mediumDistance, hit.distance);
                    }
                    _target = transform.position + laneDirection * (transform.position - _currentLane.EndPoint).magnitude;
                }
                rayOrigin += transform.forward * step;
            }
        }

        // check for objects ahead
        int ahead = CheckAhead();
        if (ahead == 2)
        {
            ExitState(HFSM_State.BASE_MERGE_ONTO_LANE);
            EnterState(HFSM_State.BASE_EVADE_OBSTACLE);
            return;
        }
        if (ahead != 0)
        {
            _minSpeed = 0;
        }
        else if (_target == _currentLane.StartPoint)
        {
            _maxSpeed = _currentLane.StartIntersection.SpeedLimit;
            _minSpeed = _currentLane.StartIntersection.SpeedLimit * 0.5f;
        }
        else
            _minSpeed = _currentLane.SpeedLimit * 0.5f;

        Seek();
    }

    /// <summary>
    /// Update for agent in approach intersection state
    /// </summary>
    private void UpdateApproachIntersection()
    {
        //Debug.Log($"{Time.frameCount} UpdateApproachIntersection");
        _target = _currentLane.EndPoint;
        bool canCross = CheckRightOfWay();
        //Debug.Log($"{name} can cross: {canCross}");
        float delta = (_currentLane.EndPoint - transform.position).magnitude;
        if (delta < _mediumDistance)
        {
            ExitState(HFSM_State.BASE_APPROACH_INTERSECTION);
            if (canCross)
                EnterState(HFSM_State.BASE_CROSS_INTERSECTION);
            else
                EnterState(HFSM_State.BASE_WAIT_AT_INTERSECTION);
            return;
        }
        // TODO: Check for right of way
        // TODO: Check for Obstacles in front
        // TODO: Keep distance to other vehicles and overtake
        // check for objects ahead
        int ahead = CheckAhead();
        if (ahead == 2)
        {
            ExitState(HFSM_State.BASE_APPROACH_INTERSECTION);
            EnterState(HFSM_State.BASE_EVADE_OBSTACLE);
            return;
        }

        _minDistance = _closeDistance;
        _minSpeed = 0;

        Seek();
    }

    /// <summary>
    /// Update for agent in wait at intersection state
    /// </summary>
    private void UpdateWaitIntersection()
    {
        _target = _currentLane.EndPoint;
        //Debug.Log($"{Time.frameCount} UpdateWaitIntersection");
        bool canCross = CheckRightOfWay();
        // TODO: Check for right of way
        if (canCross)
        {
            ExitState(HFSM_State.BASE_WAIT_AT_INTERSECTION);
            EnterState(HFSM_State.BASE_CROSS_INTERSECTION);
        }
        else
        {
            _minSpeed = 0;
            if (CheckAhead() != 0)
                _minDistance = _closeDistance;
            else
                _minDistance = 0;
            Seek();
        }
    }

    private bool CheckRightOfWay()
    {
        if ((IsInState(HFSM_State.BASE_DRUNK) && UnityEngine.Random.Range(0f, 1f) < _drunkLawRandom) || IsInState(HFSM_State.BASE_RACER) || IsInState(HFSM_State.BASE_CHASE))
            return true;
        if (IsInState(HFSM_State.BASE_DRUNK) && UnityEngine.Random.Range(0f, 1f) < _drunkLawRandom)
            return false;

        bool canCross = true;
        bool checkLights;
        TrafficlightController trafficLightController = _currentLane.EndIntersection.TrafficlightController;
        Trafficlight trafficLight = trafficLightController.GetTrafficlight(_currentLane.Heading);
        checkLights = trafficLight != null;

        if (checkLights)
        {
            canCross = trafficLight.IsGreen;
        }
        else
        {
            foreach (FiniteStateAgent agent in _currentLane.EndIntersection.ApproachingAgents)
            {
                if (agent.CurrentLane.Angle(_currentLane) == 90 && agent != this)
                {
                    float thisDistance = (transform.position - _currentLane.EndIntersection.transform.position).magnitude;
                    float otherDistance = (agent.transform.position - agent.CurrentLane.EndIntersection.transform.position).magnitude;
                    if (otherDistance < thisDistance)
                    {
                        canCross = false;
                        break;
                    }
                }
            }
        }

        return canCross;
    }

    /// <summary>
    /// Update for agent in cross intersection state
    /// </summary>
    private void UpdateCrossIntersection()
    {
        //Debug.Log($"{Time.frameCount} UpdateCrossIntersection");
        //Debug.Log($"{Time.frameCount} current lane: {_currentLane} next lane: {_nextLane} enter: {_enterIntersection}");
        _maxSpeed = _nextLane.StartIntersection.SpeedLimit;
        _minSpeed = _nextLane.StartIntersection.SpeedLimit * 0.9f;
        if (_enterIntersection)
        {
            _target = _nextLane.StartPoint;
            // if the next lane isn't parallel the waypoint is on the intersection of current and next lane
            if (_currentLane.Dot(_nextLane) == 0)
            {
                Vector3? intersection = _currentLane.Intersection(_nextLane);
                if (intersection != null)
                    _target = intersection.Value;
            }
            float delta = (_target - transform.position).magnitude;
            //Debug.Log($"{Time.frameCount} distance to center: {delta}");
            if (delta < _closeDistance)
                _enterIntersection = false;
            else
            {
                // check for objects ahead
                int ahead = CheckAhead();
                if (ahead == 2)
                {
                    ExitState(HFSM_State.BASE_CROSS_INTERSECTION);
                    EnterState(HFSM_State.BASE_EVADE_OBSTACLE);
                    return;
                }
                _minDistance = _closeDistance;
                Seek();
            }
        }
        else
        {
            float delta = (_nextLane.StartPoint - transform.position).magnitude;
            _target = _nextLane.StartPoint;
            //Debug.Log($"{Time.frameCount} distance to next lane: {delta}");
            if (delta < _mediumDistance)
            {
                ExitState(HFSM_State.BASE_CROSS_INTERSECTION);
                EnterState(HFSM_State.BASE_FOLLOW_LANE);
            }
            else
            {
                // check for objects ahead
                int ahead = CheckAhead();
                if (ahead == 2)
                {
                    ExitState(HFSM_State.BASE_CROSS_INTERSECTION);
                    EnterState(HFSM_State.BASE_EVADE_OBSTACLE);
                    return;
                }
                _minDistance = _closeDistance;
                Seek();
            }
        }

    }

    /// <summary>
    /// Update for agent in calculate route state
    /// </summary>
    private void UpdateCalculateRoute()
    {
        //Debug.Log($"{Time.frameCount} UpdateCalculateRoute");
        // TODO
    }

    /// <summary>
    /// Update for agent in waypoint reached state
    /// </summary>
    private void UpdateWaypointReached()
    {
        Debug.Log($"{Time.frameCount} UpdateWaypointReached");
        _minDistance = 0.5f;
        _minSpeed = 7;
        _target = _waypoint;
        Seek();

        //TODO: check for parking


        float delta = (_waypoint - transform.position).magnitude;
        if (delta < _minDistance)
        {
            _minSpeed = 0;
            _seekWaypoint = false;
            _drive = false;
            ExitState(HFSM_State.BASE_WAYPOINT_REACHED);
            EnterState(HFSM_State.IDLE);
        }

    }

    /// <summary>
    /// Selects the next lane
    /// </summary>
    private void SelectNextLane()
    {
        //Debug.Log($"{Time.frameCount} SelectNextLane");
        if (IsInState(HFSM_State.BASE_FOLLOW_WAYPOINT))
        {
            _nextLane = _route.NextLane();
            if (_nextLane == null)
            {
                _nextLane = GameManager.Instance.RoadGraph.GetClosestLane(_waypoint);
            }
        }
        else
        {
            _nextLane = GameManager.Instance.RoadGraph.GetRandomLane(_currentLane);
            //Debug.Log($"{Time.frameCount} current lane: {_currentLane} next lane: {_nextLane}");
        }
        ExitState(HFSM_State.BASE_SELECT_NEXT_LANE);
        EnterState(HFSM_State.BASE_MERGE_ONTO_LANE);
    }

    /// <summary>
    /// Check if the internal state is nested inside a state.
    /// </summary>
    /// <param name="state">test if internal state (_state) is nested in this</param>
    /// <returns>
    /// <para>
    /// true if _state is nested within state
    /// </para>
    /// <para>
    /// false else
    /// </para>
    /// </returns>
    private bool IsInState(HFSM_State state)
    {
        //Debug.Log($"{Time.frameCount} IsInState {state}");
        return ((int)_state & (int)state) == (int)state;
    }
    private void CheckDrivers()
    {
        if (IsInState(HFSM_State.BASE_EMERGENCY))
        {
            List<FiniteStateAgent> suspects = GameManager.Instance.Agents.Where(x => x.IsDrunk || x.IsRacer).ToList();
            suspects = suspects.Where(x => (x.transform.position - transform.position).magnitude < _viewDistance).ToList();
            if (suspects.Count > 0)
                _suspect = suspects.First();
            else
                _suspect = null;
        }
        if (IsInState(HFSM_State.BASE_RACER))
        {
            List<FiniteStateAgent> officers = GameManager.Instance.Agents.Where(x => x.IsEmergency).ToList();
            officers = officers.Where(x => (x.transform.position - transform.position).magnitude < _viewDistance).ToList();
            if (officers.Count > 0)
                _officer = officers.First();
            else
                _officer = null;
        }
    }
    #endregion

    #region Autonomous Agent Update
    private void Seek()
    {
        //Debug.Log($"{Time.frameCount} Seek {_target}");
        Vector3 delta = _target - (transform.position + transform.forward * _length / 2);
        float distance = delta.magnitude;
        float desiredSpeed = _maxSpeed * SquidMath.Smoothstep(0, _viewDistance, distance + _minDistance);

        if (distance < _minDistance)
            desiredSpeed = 0;
        desiredSpeed = Mathf.Max(_minSpeed, desiredSpeed);

        Vector3 desiredDirection = delta.normalized;

        //Debug.Log($"{name} min distance: {_minDistance} distance: {distance} min speed: {_minSpeed} desired Speed {desiredSpeed}");

        Steer(desiredDirection);
        desiredSpeed = desiredSpeed * SquidMath.Smoothstep(0, 1, -Mathf.Abs(_inputs.steerInput) + 1.5f);
        //Debug.Log($"{name} steer: {_inputs.steerInput} desired Speed {desiredSpeed}");
        Accelerate(desiredSpeed);
    }

    private void Accelerate(float desiredSpeed)
    {
        float delta = Mathf.Abs(desiredSpeed - _speed);
        //Debug.Log($"{name} speed: {_speed} desired Speed {desiredSpeed} min speed: {_minSpeed} speed Tolerance: {_speedTolerance} _eps {_eps}");
        //Debug.Log($"{name} {_speed} < {desiredSpeed - _eps}: {_speed < desiredSpeed - _eps}\n{desiredSpeed} < {_eps} && {_speed} > {desiredSpeed + _eps} {desiredSpeed < _eps && _speed > desiredSpeed + _eps}\n{_speed} > {desiredSpeed + _speedTolerance + _eps} {_speed > desiredSpeed + _speedTolerance + _eps}");
        // current speed is below desired: accelerate
        if (_speed < desiredSpeed - _eps)
        {
            _inputs.throttleInput = SquidMath.Smoothstep(0, _accelerationSmoothness, delta);
            if (IsInState(HFSM_State.BASE_DRUNK) && UnityEngine.Random.Range(0f, 1f) < _drunkAccelerateRandom)
                _inputs.throttleInput = 1;
            _inputs.brakeInput = 0;
        }
        // current speed is above desired: decelerate
        else if (_speed > desiredSpeed + _speedTolerance + _eps)
        {
            _inputs.throttleInput = 0;
            _inputs.brakeInput = 1 - SquidMath.Smoothstep(0, _breakSmoothness, delta);
            if (IsInState(HFSM_State.BASE_DRUNK) && UnityEngine.Random.Range(0f, 1f) < _drunkAccelerateRandom)
                _inputs.brakeInput = 1;
        }
        // if desired speed is approximately 0: brake
        else if (desiredSpeed < _eps && _speed > desiredSpeed + _eps)
        {
            _inputs.throttleInput = 0;
            _inputs.brakeInput = 1;
        }
        // current speed is approximately desired: do nothing
        else
        {
            _inputs.throttleInput = 0;
            _inputs.brakeInput = 0;
        }

    }

    private void Steer(Vector3 desiredDirection)
    {
        //Debug.Log($"{Time.frameCount} Steer {desiredDirection}");
        desiredDirection = Vector3.ProjectOnPlane(desiredDirection, Vector3.up);
        Vector3 heading = _rigidbody.velocity;
        heading = Vector3.ProjectOnPlane(heading, Vector3.up);
        float angle = Vector3.SignedAngle(heading, desiredDirection, Vector3.up);
        _inputs.steerInput = 2 * SquidMath.Smoothstep(-_steerSmoothness, _steerSmoothness, angle) - 1;
        if (IsInState(HFSM_State.BASE_DRUNK) && UnityEngine.Random.Range(0f, 1f) < _drunkSteerRandom)
        {
            _inputs.steerInput = Mathf.Sin(Time.realtimeSinceStartup);
        }
        if (_speed < _eps)
            _inputs.steerInput = 0;

        //Debug.Log($"{Time.frameCount} desiredDirection {desiredDirection} heading: {heading} angle: {angle} step: {Smoothstep(-_steerSensitivity, _steerSensitivity, angle)}");
    }

    #endregion

    #region Debug

    private string StateToString()
    {
        string state = $"{Time.frameCount} {name} in states: ";
        var states = Enum.GetValues(typeof(HFSM_State));
        foreach (var cur in states)
        {
            if (IsInState((HFSM_State)cur))
            {
                state += $"{(HFSM_State)cur}, ";
            }
        }
        return state;
    }

    #endregion

    #endregion

    #region Serialized Fields
    [Header("Settings")]
    [SerializeField] protected bool _drive;
    [SerializeField] protected bool _chase;
    [SerializeField] protected bool _flee;
    [SerializeField] protected bool _isCivilian;
    [SerializeField] protected bool _isDrunk;
    [SerializeField] protected bool _isEmergency;
    [SerializeField] protected bool _isRacer;
    [SerializeField] protected bool _makeWay;
    [SerializeField] protected bool _park;
    [SerializeField] protected bool _enterIntersection;
    [SerializeField] protected bool _seekWaypoint;
    [SerializeField] protected float _closeDistance;
    [SerializeField] protected float _mediumDistance;
    [SerializeField] protected float _farDistance;
    [SerializeField] protected float _viewDistance;
    [SerializeField] protected float _speedTolerance;
    [SerializeField] protected float _steerSmoothness;
    [SerializeField] protected float _accelerationSmoothness;
    [SerializeField] protected float _breakSmoothness;
    [SerializeField] protected float _chaseSearchTime;
    [SerializeField] protected float _drunkAccelerateRandom;
    [SerializeField] protected float _drunkSteerRandom;
    [SerializeField] protected float _drunkLawRandom;
    [SerializeField] protected Vector3 _waypoint;

    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields
    private float _eps = 0.5f;
    private float _maxSpeed;
    private float _minSpeed;
    private float _speed;
    private float _suspectLastSeen;
    private float _minDistance;
    private Lane _currentLane;
    private Lane _nextLane;
    private HFSM_State _state;
    private Vector3 _lastPositionEvade;
    private Route _route;
    private Vector3 _target;
    private FiniteStateAgent _suspect;
    private FiniteStateAgent _officer;

    #endregion

    #region Public Fields
    public bool Drive { get { return _drive; } set { _drive = value; } }
    public bool Chase { get { return _chase; } set { _chase = value; } }
    public bool Flee { get { return _flee; } set { _flee = value; } }
    public bool IsCivilian { get { return _isCivilian; } set { _isCivilian = value; } }
    public bool IsDrunk { get { return _isDrunk; } set { _isDrunk = value; } }
    public bool IsEmergency { get { return _isEmergency; } set { _isEmergency = value; } }
    public bool IsRacer { get { return _isRacer; } set { _isRacer = value; } }
    public bool MakeWay { get { return _makeWay; } set { _makeWay = value; } }
    public bool Park { get { return _park; } set { _park = value; } }
    public float Speed { get { return _speed; } }
    public float ViewDistance { get { return _viewDistance; } }
    public Lane CurrentLane { get { return _currentLane; } }

    #endregion
}

using System.Collections.Generic;
using UnityEngine;

public class Intersection : MonoBehaviour
{
    #region Unity Messages
    private void OnEnable()
    {
        _approachingAgents = new();
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, 1);
        Gizmos.color = new Color(1, 0.5f, 0);
        foreach (Lane incoming in _incomingLanes)
        {
            Gizmos.color = new Color(Mathf.Sin(_incomingLanes.IndexOf(incoming)), Mathf.Atan2(Gizmos.color.r, Gizmos.color.b + Mathf.Epsilon), Mathf.Cos(_incomingLanes.IndexOf(incoming)));
            List<Lane> connections = GetConnections(incoming);
            if (connections.Count == 0) break;
            foreach (Lane outgoing in connections)
            {
                Gizmos.DrawLine(incoming.EndPoint, outgoing.StartPoint);
            }
        }
        /*Lane[] lanes = (Lane[])FindObjectsOfType(typeof(Lane));
        foreach (Lane lane in lanes)
        {
            if (lane.StartIntersection == this && !_outgoingLanes.Contains(lane))
                _outgoingLanes.Add(lane);
            if (lane.EndIntersection == this && !_incomingLanes.Contains(lane))
                _incomingLanes.Add(lane);
        }*/
        /* _outgoingLanes = new();
         _incomingLanes = new();*/
    }
    #endregion

    #region Public Methods
    public List<Lane> GetConnections(Lane incoming)
    {
        List<Lane> outgoing = new();
        if (_connectionsStart.Count == 0 || _connectionsEnd.Count == 0 || _connectionsStart.Count != _connectionsEnd.Count) return outgoing;
        int incomingIndex = _incomingLanes.IndexOf(incoming);
        for (int i = 0; i < _connectionsStart.Count; i++)
        {
            int startIndex = _connectionsStart[i];
            if (startIndex == incomingIndex)
            {
                int endIndex = _connectionsEnd[i];
                if (endIndex >= _outgoingLanes.Count) continue;
                outgoing.Add(_outgoingLanes[endIndex]);
            }
        }
        return outgoing;
    }
    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods

    #endregion

    #region Serialized Fields
    [Header("Settings")]
    [SerializeField] private float _speedLimit;
    [SerializeField] private List<Lane> _incomingLanes;
    [SerializeField] private List<Lane> _outgoingLanes;
    [SerializeField] private List<int> _connectionsStart;
    [SerializeField] private List<int> _connectionsEnd;
    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields
    private List<FiniteStateAgent> _approachingAgents;
    #endregion

    #region Public Fields
    public float SpeedLimit { get { return _speedLimit; } }
    public List<Lane> IncomingLanes { get { return _incomingLanes; } }
    public List<Lane> OutgoingLanes { get { return _outgoingLanes; } }
    public List<FiniteStateAgent> ApproachingAgents { get { return _approachingAgents; } set { _approachingAgents = value; } }
    public TrafficlightController TrafficlightController { get { return GetComponent<TrafficlightController>(); } }
    #endregion
}

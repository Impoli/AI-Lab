using System.Collections.Generic;
using UnityEngine;

public class Lane : MonoBehaviour
{
    #region Unity Messages
    private void Start()
    {
        _startPoint = transform.position - _direction.normalized * _length / 2;
        _endPoint = transform.position + _direction.normalized * _length / 2;
    }

    private void OnDrawGizmos()
    {
        _startPoint = transform.position - _direction.normalized * _length / 2;
        _endPoint = transform.position + _direction.normalized * _length / 2;
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(_startPoint, 1);
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_endPoint, 1);
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(_startPoint, _endPoint);
    }
    #endregion

    #region Public Methods
    public Vector3 GetClosestPoint(Vector3 point)
    {
        Vector3 start_end = _endPoint - _startPoint;
        Vector3 end_start = _startPoint - _endPoint;
        Vector3 u = point - _startPoint;
        Vector3 w = Vector3.Project(u, start_end.normalized);
        Vector3 closest = _startPoint + w;

        // closest is beyond start point
        if (Vector3.Dot(closest - _startPoint, start_end) > 0)
        {
            // closest is beyond end point
            if (Vector3.Dot(closest - _endPoint, end_start) < 0)
            {
                closest = _endPoint;
            }
            // else point is on line segment
        }
        // closest is before start
        else
        {
            closest = _startPoint;
        }

        return closest;
    }

    public float GetDistanceToCenter(Vector3 point)
    {
        Vector3 delta = GetClosestPoint(point) - point;
        return delta.magnitude;
    }

    public float Dot(Lane other)
    {
        Vector3 thisVector = EndPoint - StartPoint;
        Vector3 otherVector = other.EndPoint - other.StartPoint;
        return Vector3.Dot(thisVector, otherVector);
    }

    public float Angle(Lane other)
    {
        Vector3 thisVector = EndPoint - StartPoint;
        Vector3 otherVector = other.EndPoint - other.StartPoint;
        return Vector3.Angle(thisVector, otherVector);
    }

    public Vector3? Intersection(Lane other)
    {
        if (Angle(other) % 180 == 0) return null;

        float a = (StartPoint.x - other.StartPoint.x) * (other.StartPoint.z - other.EndPoint.z) - (StartPoint.z - other.StartPoint.z) * (other.StartPoint.x - other.EndPoint.x);
        float b = (StartPoint.x - EndPoint.x) * (other.StartPoint.z - other.EndPoint.z) - (StartPoint.z - EndPoint.z) * (other.StartPoint.x - other.EndPoint.x);

        float t = a / b;

        return new(StartPoint.x + t * (EndPoint.x - StartPoint.x), 0, StartPoint.z + t * (EndPoint.z - StartPoint.z));
    }
    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods


    #endregion

    #region Serialized Fields
    [Header("Settings")]
    [SerializeField] private Heading _heading;
    [SerializeField] private float _width;
    [SerializeField] private float _speedLimit;
    [SerializeField] private float _length;
    [SerializeField] private Vector3 _direction;
    [SerializeField] private List<Lane> _parallelLanes;
    [SerializeField] private List<Lane> _opposingLanes;
    [SerializeField] private Intersection _startIntersection;
    [SerializeField] private Intersection _endIntersection;
    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields
    private Vector3 _startPoint;
    private Vector3 _endPoint;
    #endregion

    #region Public Fields
    public float Width { get { return _width; } }
    public float SpeedLimit { get { return _speedLimit; } }
    public float Length { get { return _length; } }
    public Vector3 StartPoint { get { return _startPoint; } }
    public Vector3 EndPoint { get { return _endPoint; } }
    public Vector3 Direction { get { return _direction; } }
    public List<Lane> ParallelLanes { get { return _parallelLanes; } }
    public List<Lane> OpposingLanes { get { return _opposingLanes; } }
    public Heading Heading { get { return _heading; } }
    public Intersection StartIntersection { get { return _startIntersection; } }
    public Intersection EndIntersection { get { return _endIntersection; } }
    #endregion
}

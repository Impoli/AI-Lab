using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class Route
{
    #region Structs
    // TODO: Make these into Objects and run BuildMap once in RoadGraph
    private struct Edge
    {
        public Edge(Lane lane, int id)
        {
            float length = lane.Length;
            float speedLimit = lane.SpeedLimit;
            float discount = Cost = -SquidMath.Smoothstep(0, 200, speedLimit) * length;
            Cost = length + discount;
            Lane = lane;
            StartNode = -1;
            EndNode = -1;
            ID = id;
        }
        public float Cost;
        public Lane Lane;
        public int StartNode;
        public int EndNode;
        public int ID;
    }
    private struct Node
    {
        public Node(Vector3 position, Intersection intersection, int id)
        {
            Position = position;
            Intersection = intersection;
            Visited = false;
            HDistance = float.PositiveInfinity;
            GDistance = float.PositiveInfinity;
            InEdges = new();
            OutEdges = new();
            ID = id;
            NearestToStart = -1;
        }
        public bool Visited;
        public float HDistance;
        public float GDistance;
        public Vector3 Position;
        public List<int> InEdges;
        public List<int> OutEdges;
        public Intersection Intersection;
        public int ID;
        public int NearestToStart;
    }
    private struct Map
    {
        public List<Edge> Edges;
        public List<Node> Nodes;

        public Node GetNodeByID(int id)
        {
            return Nodes.Where(x => x.ID == id).FirstOrDefault();
        }
        public Edge GetEdgeByID(int id)
        {
            return Edges.Where(x => x.ID == id).FirstOrDefault();
        }
        public void SetNodeByID(int id, Node node)
        {
            Node original = GetNodeByID(id);
            Nodes[Nodes.IndexOf(original)] = node;
        }
        public void SetEdgeByID(int id, Edge edge)
        {
            Edge original = GetEdgeByID(id);
            Edges[Edges.IndexOf(original)] = edge;
        }
    }
    #endregion
    #region Constructor
    public Route(Vector3 start, Vector3 end)
    {
        BuildMap();
        CalculateRoute(start, end);
    }
    #endregion
    #region Public Methods
    public Lane NextLane()
    {
        if (_route.Count == 1)
            return null;
        Node currentNode = _route.First();
        _route.Remove(currentNode);
        Node nextNode = _route.First();
        int nextEdgeID = currentNode.OutEdges.Where(x => nextNode.InEdges.Contains(x)).First();
        Edge nextEdge = _map.GetEdgeByID(nextEdgeID);
        return nextEdge.Lane;
    }

    public List<Vector3> GetRoute()
    {
        List<Vector3> route = new();
        foreach (Node node in _route)
        {
            route.Add(node.Position);
        }
        return route;
    }
    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods
    private void CalculateRoute(Vector3 start, Vector3 end)
    {
        Lane startLane = GameManager.Instance.RoadGraph.GetClosestLane(start);
        Lane endLane = GameManager.Instance.RoadGraph.GetClosestLane(end);
        // current intersection is start lane's snd / end lane's start intersection if they have the same name (for this to be true each intersection game object must have unique name!)
        Node startNode = _map.Nodes.Where(x => string.Compare(x.Intersection.name, startLane.EndIntersection.name) == 0).FirstOrDefault();
        Node endNode = _map.Nodes.Where(x => string.Compare(x.Intersection.name, endLane.StartIntersection.name) == 0).FirstOrDefault();
        _route = GetShortestPathAstar(startNode.ID, endNode.ID);
    }
    // A* https://www.codeproject.com/Articles/1221034/Pathfinding-Algorithms-in-Csharp
    private List<Node> GetShortestPathAstar(int startNodeID, int endNodeID)
    {
        ResetNodes();
        // calculate the heuristic estimated distance from each node to the end node.
        for (int i = 0; i < _map.Nodes.Count; i++)
        {
            Node node = _map.Nodes[i];
            node.HDistance = DistanceHeuristic(node.ID, endNodeID);
            _map.Nodes[i] = node;
        }

        AstarSearch(startNodeID, endNodeID);
        List<Node> shortestPath = BuildShortestPath(startNodeID, endNodeID);
        shortestPath.Reverse();
        return shortestPath;
    }

    private void AstarSearch(int startNodeID, int endNodeID)
    {
        Node startNode = _map.GetNodeByID(startNodeID);
        startNode.GDistance = 0;
        startNode.NearestToStart = startNode.ID;
        _map.SetNodeByID(startNodeID, startNode);
        List<int> prioQueue = new() { startNodeID };
        do
        {
            prioQueue.OrderBy(x => _map.GetNodeByID(x).HDistance + _map.GetNodeByID(x).GDistance).ToList();
            int currentNodeID = prioQueue.First();
            prioQueue.Remove(currentNodeID);
            List<int> outEdges = _map.GetNodeByID(currentNodeID).OutEdges.OrderBy(x => _map.GetEdgeByID(x).Cost).ToList();
            foreach (int outgoingID in outEdges)
            {
                int childNodeID = _map.GetEdgeByID(outgoingID).EndNode;
                if (_map.GetNodeByID(childNodeID).Visited)
                    continue;

                float childGDist = _map.GetNodeByID(childNodeID).GDistance;
                float currentGDist = _map.GetNodeByID(currentNodeID).GDistance;
                float edgeLength = _map.GetEdgeByID(outgoingID).Cost;
                bool noGDist = childGDist == float.PositiveInfinity;
                bool shorterG = currentGDist + edgeLength < childGDist;

                if (noGDist || shorterG)
                {
                    Node childNode = _map.GetNodeByID(childNodeID);
                    childNode.GDistance = childGDist;
                    childNode.NearestToStart = currentNodeID;
                    _map.SetNodeByID(childNodeID, childNode);
                    if (!prioQueue.Contains(childNodeID))
                        prioQueue.Add(childNodeID);
                }
            }
            Node currentNode = _map.GetNodeByID(currentNodeID);
            currentNode.Visited = true;
            _map.SetNodeByID(currentNodeID, currentNode);
            if (currentNodeID == endNodeID)
                return;
        } while (prioQueue.Count > 0);
    }

    private List<Node> BuildShortestPath(int startNodeID, int endNodeID)
    {
        List<Node> path = new();
        int currentNodeID = endNodeID;
        int c = 0;
        while (true)
        {
            Node currentNode = _map.GetNodeByID(currentNodeID);
            if (currentNode.ID == startNodeID)
            {
                path.Add(currentNode);
                break;
            }
            path.Add(currentNode);
            currentNodeID = currentNode.NearestToStart;
            c++;
            if (c > 1000)
                throw new Exception("Build Shortest Path ran into an endless loop!");
        }

        return path;
    }

    private float DistanceHeuristic(int currentID, int endID)
    {
        Node current = _map.GetNodeByID(currentID);
        Node end = _map.GetNodeByID(endID);
        return (end.Position - current.Position).magnitude;
    }

    private void ResetNodes()
    {
        for (int i = 0; i < _map.Nodes.Count; i++)
        {
            Node current = _map.Nodes[i];
            current.Visited = false;
            current.HDistance = float.PositiveInfinity;
            current.GDistance = float.PositiveInfinity;
            current.NearestToStart = -1;
            _map.Nodes[i] = current;
        }
    }
    private void BuildMap()
    {
        _map = new();
        _map.Nodes = new();
        _map.Edges = new();

        // create edges
        foreach (Lane lane in GameManager.Instance.RoadGraph.Lanes)
        {
            _map.Edges.Add(new(lane, _map.Edges.Count));
        }

        // create nodes
        foreach (Intersection intersection in GameManager.Instance.RoadGraph.Intersections)
        {
            _map.Nodes.Add(new(intersection.transform.position, intersection, _map.Nodes.Count));
        }

        // walk through graph and find all edges and connect nodes
        foreach (Node currentNode in _map.Nodes)
        {
            if (currentNode.OutEdges.Count == 0)
            {
                // find outgoing edges
                foreach (Lane outgoingLane in currentNode.Intersection.OutgoingLanes)
                {
                    for (int j = 0; j < _map.Edges.Count; j++)
                    {
                        Edge currentEdge = _map.Edges[j];
                        // current edge is outgoing lane if they have the same name (for this to be true each lane game object must have unique name!)
                        if (string.Compare(outgoingLane.name, currentEdge.Lane.name, true) == 0)
                        {
                            if (!currentNode.OutEdges.Contains(currentEdge.ID))
                            {
                                currentNode.OutEdges.Add(currentEdge.ID);
                                Edge tmp = currentEdge;
                                tmp.StartNode = currentNode.ID;
                                _map.Edges[j] = tmp;
                            }

                        }
                    }
                }
            }

            if (currentNode.InEdges.Count == 0)
            {
                // find incoming edges
                foreach (Lane incomingLane in currentNode.Intersection.IncomingLanes)
                {
                    for (int j = 0; j < _map.Edges.Count; j++)
                    {
                        Edge currentEdge = _map.Edges[j];
                        // current edge is outgoing lane if they have the same name (for this to be true each lane game object must have unique name!)
                        if (string.Compare(incomingLane.name, currentEdge.Lane.name, true) == 0)
                        {
                            if (!currentNode.InEdges.Contains(currentEdge.ID))
                            {
                                currentNode.InEdges.Add(currentEdge.ID);
                                Edge tmp = currentEdge;
                                tmp.EndNode = currentNode.ID;
                                _map.Edges[j] = tmp;
                            }

                        }
                    }
                }
            }

        }
    }
    #endregion

    #region Serialized Fields

    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields

    private Map _map;
    private List<Node> _route;
    #endregion

    #region Public Fields

    #endregion
}

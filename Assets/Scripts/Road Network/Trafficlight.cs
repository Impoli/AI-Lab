using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Controls;

public class Trafficlight : MonoBehaviour
{
    #region Unity Messages

    private void Update()
    {
        switch (_state)
        {
            case 0:
                _green.enabled = true;
                _yellow.enabled = false;
                _red.enabled = false;
                break;
            case 1:
                _green.enabled = false;
                _yellow.enabled = true;
                _red.enabled = false;
                break;
            case 2:
                _green.enabled = false;
                _yellow.enabled = false;
                _red.enabled = true;
                break;
            default:
                Debug.LogError($"{name} invalid state {_state}");
                _green.enabled = false;
                _yellow.enabled = true;
                _red.enabled = false;
                break;
        }

        if (_switch)
        {
            float delta = Time.realtimeSinceStartup - _switchStart;
            if (delta > _yellowTime)
            {
                switch (_lastState)
                {
                    case 0:
                        _state = 2;
                        break;
                    case 2:
                        _state = 0;
                        break;
                    default:
                        _state = -1;
                        Debug.LogError($"{name} invalid last state {_lastState}");
                        break;
                }
                _switch = false;
            }
        }
    }
    #endregion

    #region Public Methods
    public void Switch()
    {
        _lastState = _state;
        _state = 1;
        _switch = true;
        _switchStart = Time.realtimeSinceStartup;
    }
    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods

    #endregion

    #region Serialized Fields
    [Header("Settings")]
    [SerializeField] float _yellowTime;

    [Header("Lights")]
    [SerializeField] Light _green;
    [SerializeField] Light _yellow;
    [SerializeField] Light _red;
    #endregion

    #region Protected Fields
    #endregion

    #region Private Fields
    private int _state; // 0 = green, 1 = yellow, 2 = red
    private int _lastState;
    private bool _switch;
    private float _switchStart;
    #endregion

    #region Public Fields
    public bool IsGreen { get { return _state == 0; } }
    public int State { get { return _state; } set { _state = value; } }
    #endregion
}

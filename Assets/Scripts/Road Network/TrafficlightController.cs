using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TrafficlightController : MonoBehaviour
{
    #region Unity Messages
    protected void OnEnable()
    {
        foreach (Trafficlight light in _lightsNorth)
        {
            light.State = 2;
        }
        foreach (Trafficlight light in _lightsSouth)
        {
            light.State = 2;
        }
        foreach (Trafficlight light in _lightsEast)
        {
            light.State = 0;
        }
        foreach (Trafficlight light in _lightsWest)
        {
            light.State = 0;
        }
        _timerCoroutine = WaitAndSwitch(_phaseDuration);
        StartCoroutine(_timerCoroutine);
    }

    private void OnDisable()
    {
        StopCoroutine(_timerCoroutine);
    }
    #endregion

    #region Public Methods
    public Trafficlight GetTrafficlight(Heading heading)
    {
        switch (heading)
        {
            case Heading.NORTH:
                if (_lightsSouth.Count > 0)
                    return _lightsSouth[0];
                break;
            case Heading.EAST:
                if (_lightsWest.Count > 0)
                    return _lightsWest[0];
                break;
            case Heading.SOUTH:
                if (_lightsNorth.Count > 0)
                    return _lightsNorth[0];
                break;
            case Heading.WEST:
                if (_lightsEast.Count > 0)
                    return _lightsEast[0];
                break;
            default:
                return null;
        }
        return null;
    }
    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods
    private IEnumerator WaitAndSwitch(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            foreach (Trafficlight light in _lightsNorth)
            {
                light.Switch();
            }
            foreach (Trafficlight light in _lightsSouth)
            {
                light.Switch();
            }
            foreach (Trafficlight light in _lightsEast)
            {
                light.Switch();
            }
            foreach (Trafficlight light in _lightsWest)
            {
                light.Switch();
            }
        }
    }
    #endregion

    #region Serialized Fields
    [Header("Settings")]
    [SerializeField] private float _phaseDuration;
    [SerializeField] private List<Trafficlight> _lightsNorth;
    [SerializeField] private List<Trafficlight> _lightsSouth;
    [SerializeField] private List<Trafficlight> _lightsEast;
    [SerializeField] private List<Trafficlight> _lightsWest;

    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields
    private IEnumerator _timerCoroutine;
    #endregion

    #region Public Fields
    #endregion
}

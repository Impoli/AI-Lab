using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoadGraph : MonoBehaviour
{

    #region Unity Messages
    private void OnDrawGizmos()
    {/*
        if (_map.Nodes == null || _map.Edges == null) return;
        Gizmos.color = Color.red;
        foreach (Node node in _map.Nodes)
        {
            Gizmos.DrawSphere(node.Position + Vector3.up * 2, 1);
            foreach (int outgoingID in node.OutEdges)
            {
                Edge outgoing = _map.Edges.Where(x => x.ID == outgoingID).FirstOrDefault();
                Node start = _map.Nodes.Where(x => x.ID == outgoing.StartNode).FirstOrDefault();
                Node end = _map.Nodes.Where(x => x.ID == outgoing.EndNode).FirstOrDefault();
                Gizmos.DrawLine(start.Position + Vector3.up * 2, end.Position + Vector3.up * 2);
            }
        }
        Gizmos.color = Color.green;
        foreach (Node node in _map.Nodes)
        {
            Gizmos.DrawSphere(node.Position + Vector3.up * 4, 1);
            foreach (int incomingID in node.InEdges)
            {
                Edge incoming = _map.Edges.Where(x => x.ID == incomingID).FirstOrDefault();
                Node start = _map.Nodes.Where(x => x.ID == incoming.StartNode).FirstOrDefault();
                Node end = _map.Nodes.Where(x => x.ID == incoming.EndNode).FirstOrDefault();
                Gizmos.DrawLine(start.Position + Vector3.up * 4, end.Position + Vector3.up * 4);
            }
        }
        Gizmos.color = Color.blue;
        foreach (Edge edge in _map.Edges)
        {
            Node start = _map.Nodes.Where(x => x.ID == edge.StartNode).FirstOrDefault();
            Node end = _map.Nodes.Where(x => x.ID == edge.EndNode).FirstOrDefault();
            Gizmos.DrawSphere(start.Position + Vector3.up * 6, 1);
            Gizmos.DrawSphere(end.Position + Vector3.up * 6, 1);
            Gizmos.DrawLine(start.Position + Vector3.up * 6, end.Position + Vector3.up * 6);
        }*/
    }
    private void OnEnable()
    {
        _lanes = GetComponentsInChildren<Lane>().ToList();
        _intersections = GetComponentsInChildren<Intersection>().ToList();

    }
    #endregion

    #region Public Methods
    public Lane GetClosestLane(Vector3 point)
    {
        Lane closestLane = _lanes[0];
        Vector3 delta = _lanes[0].GetClosestPoint(point) - point;
        float closestDistance = delta.magnitude;
        foreach (Lane lane in _lanes)
        {
            delta = lane.GetClosestPoint(point) - point;
            if (delta.magnitude < closestDistance)
            {
                closestLane = lane;
                closestDistance = delta.magnitude;
            }
        }
        return closestLane;
    }

    public Lane GetRandomLane(Lane currentLane)
    {
        List<Lane> connections = currentLane.EndIntersection.OutgoingLanes;
        int i = UnityEngine.Random.Range(0, connections.Count);
        Lane selection = connections[i];
        while (currentLane.ParallelLanes.Contains(selection) || currentLane.OpposingLanes.Contains(selection))
        {
            selection = connections[(i + 1) % connections.Count];
        }
        return selection;
    }
    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods





    #endregion

    #region Serialized Fields
    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields
    private List<Lane> _lanes;
    private List<Intersection> _intersections;
    #endregion

    #region Public Fields
    public List<Lane> Lanes { get { return _lanes; } }
    public List<Intersection> Intersections { get { return _intersections; } }
    #endregion
}

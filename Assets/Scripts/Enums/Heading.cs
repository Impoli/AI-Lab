using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Heading
{
    _,
    NORTH,
    WEST,
    SOUTH,
    EAST,
}

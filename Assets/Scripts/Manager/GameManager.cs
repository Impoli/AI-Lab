using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Unity Messages
    private void Awake()
    {
        if (_instance == null)
            _instance = this;

        if (_instance != this)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(this);
        }
    }

    private void OnEnable()
    {
        RCCP.RegisterPlayerVehicle(_playerCar, true, true);
        _agents = new();
        _agents.AddRange(FindObjectsByType<FiniteStateAgent>(FindObjectsSortMode.None));
    }

    private void Update()
    {
        _camera.TPSDistance = _tpsDistance;
        _camera.TPSHeight = _tpsHeight;
    }
    #endregion

    #region Public Methods

    #endregion

    #region Protected Methods

    #endregion

    #region Private Methods

    #endregion

    #region Serialized Fields
    [Header("RCCP Settings")]
    [SerializeField] private RCCP_CarController _playerCar;
    [SerializeField] private RCCP_Camera _camera;
    [SerializeField] private float _tpsDistance;
    [SerializeField] private float _tpsHeight;

    [Header("References")]
    [SerializeField] private RoadGraph _roadGraph;

    #endregion

    #region Protected Fields

    #endregion

    #region Private Fields
    private static GameManager _instance;
    private List<FiniteStateAgent> _agents;

    #endregion

    #region Public Fields
    public RoadGraph RoadGraph { get { return _roadGraph; } }
    public static GameManager Instance { get { return _instance; } }
    public List<FiniteStateAgent> Agents { get { return _agents; } }
    #endregion
}

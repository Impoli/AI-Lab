//----------------------------------------------
//        Realistic Car Controller Pro
//
// Copyright � 2014 - 2024 BoneCracker Games
// https://www.bonecrackergames.com
// Ekrem Bugra Ozdoganlar
//
//----------------------------------------------

#pragma warning disable 0414

using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Mobile UI Drag used for orbiting RCCP Camera.
/// </summary>
[AddComponentMenu("BoneCracker Games/Realistic Car Controller Pro/UI/Mobile/RCCP UI Drag")]
public class RCCP_UI_Drag : RCCP_UIComponent, IDragHandler, IEndDragHandler {

    /// <summary>
    /// Dragging or pressing now?
    /// </summary>
    private bool isPressing = false;

    private void Awake() {

        //  If mobile controller is not enabled disable the gameobject and return.
        if (!RCCPSettings.mobileControllerEnabled) {

            gameObject.SetActive(false);
            return;

        }

    }

    /// <summary>
    /// While dragging.
    /// </summary>
    /// <param name="data"></param>
    public void OnDrag(PointerEventData data) {

        //  If mobile controller is not enabled, return.
        if (!RCCPSettings.mobileControllerEnabled)
            return;

        isPressing = true;

        if (RCCP_SceneManager.Instance.activePlayerCamera) {

            RCCP_SceneManager.Instance.activePlayerCamera.OnDrag(data);
            RCCP_SceneManager.Instance.activePlayerCamera.orbitHolding = true;

        }


    }

    public void OnEndDrag(PointerEventData data) {

        //  If mobile controller is not enabled, return.
        if (!RCCPSettings.mobileControllerEnabled)
            return;

        isPressing = false;

        if (RCCP_SceneManager.Instance.activePlayerCamera) {

            RCCP_SceneManager.Instance.activePlayerCamera.orbitHolding = false;

        }

    }

    private void OnDisable() {

        isPressing = false;

    }

}
